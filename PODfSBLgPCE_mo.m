%% Implement POD facilitated GPR for Liange's problem
clear all;

%% set it to true if using matlab parallel feature; otherwise set it to false
if_parallel = false;

rom_path = '/global/homes/y/yaning/repos/rbf/rom_class';
SBL_path = '/global/homes/y/yaning/repos/rbf/SB2_Release_200';
addpath(genpath(rom_path));
addpath(SBL_path);

%%--------------Need to change -------------------------
% data_path = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/';  % On Cori
data_path = '/scratch1/scratchdirs/yaning/cori_scratch/Research/Liange_sim_new/';  % On edison
%%--------------Need to change -------------------------

ne = 31320;  % number of grid cells
ntime = 21; % number of times
param_idx = [1:8 10:31];  % one parameter is not used
%%--------------Need to change -------------------------
varn = 'ph';

data_tra_fn = {['data_co2_' varn '_all_sz2400_v1.mat'], ...
               ['data_co2_' varn '_all_sz2400_v3.mat'], ...
               ['data_co2_' varn '_all_sz2400_v4.mat']};
data_val_fn = {['data_co2_' varn '_all_sz1200_v1.mat']};
%%--------------Need to change -------------------------

mask = load([data_path data_tra_fn{1}], 'mask');
mask = mask.mask;
volume_all = load([data_path data_tra_fn{1}], 'volume');
volume_all = volume_all.volume;
volume_masked = volume_all(logical(mask));
param_tra = [];
param_val = [];
param_nlz_tra = [];
param_nlz_val = [];

%% training input
for i = 1 : length(data_tra_fn)
    la = load([data_path data_tra_fn{i}], 'local_assignment');
    la = la.local_assignment;
    param_tmp = load([data_path data_tra_fn{i}], 'parameters_mapped');
    param_tmp = param_tmp.parameters_mapped;
    %% Note the local_assignment in the data is 0-based index. So +1 is needed.
    param_tra = [param_tra; param_tmp(la+1, param_idx)];
    param_tmp = load([data_path data_tra_fn{i}], 'parameters_normalized');
    param_tmp = param_tmp.parameters_normalized;
    %% Note the local_assignment in the data is 0-based index. So +1 is needed.
    param_nlz_tra = [param_nlz_tra; param_tmp(la+1, param_idx)];
end

%% validation input
for i = 1 : length(data_val_fn)
    la = load([data_path data_val_fn{i}], 'local_assignment');
    la = la.local_assignment;
    param_tmp = load([data_path data_val_fn{i}], 'parameters_mapped');
    param_tmp = param_tmp.parameters_mapped;
    %% Note the local_assignment in the data is 0-based index. So +1 is needed.
    param_val = [param_val; param_tmp(la+1, param_idx)];
    param_tmp = load([data_path data_val_fn{i}], 'parameters_normalized');
    param_tmp = param_tmp.parameters_normalized;
    %% Note the local_assignment in the data is 0-based index. So +1 is needed.
    param_nlz_val = [param_nlz_val; param_tmp(la+1, param_idx)];
end

ns_tra = size(param_tra, 1);  % number of training samples
ns_val = size(param_val, 1);  % number of validation samples
output_tra = zeros(sum(mask)*ntime, ns_tra);
output_val = zeros(sum(mask)*ntime, ns_val);

%% training output and training plume volume
%% plume volume
%%--------------Need to change -------------------------
tol_plume = 7.0;  %% for ph
% tol_plume = 3.04e-9;  %% for pb
% tol_plume = 3.19e-11;  %% for phe
% tol_plume = 1.019e-6;  %% for bar
%%--------------Need to change -------------------------

plume_tra = zeros(ns_tra, ntime);
idx1 = 1;
idx2 = 0;
for i = 1 : length(data_tra_fn)
    output_tmp = load([data_path data_tra_fn{i}], varn);
    output_tmp = output_tmp.(varn);

    for j = 1 : size(output_tmp, 3)
        for k = 1 : ntime
            op_tmp = output_tmp(logical(mask), k, j);
            if strcmp(varn, 'ph')
                vol_idx = logical(op_tmp<tol_plume);
            else
                vol_idx = logical(op_tmp>tol_plume);
            end
            plume_tra(idx2+j, k) = sum(volume_masked(vol_idx));
        end
    end

    idx2 = idx2 + size(output_tmp, 3);
    output_tra(:,idx1:idx2) = reshape(output_tmp(logical(mask), :, :), ...
                                      sum(mask)*ntime, size(output_tmp, 3));

    idx1 = idx1 + size(output_tmp, 3);
end

%% validation output and validation plume volume
plume_val = zeros(ns_val, ntime);
idx1 = 1;
idx2 = 0;
for i = 1 : length(data_val_fn)
    output_tmp = load([data_path data_val_fn{i}], varn);
    output_tmp = output_tmp.(varn);

    for j = 1 : size(output_tmp, 3)
        for k = 1 : ntime
            op_tmp = output_tmp(logical(mask), k, j);
            if strcmp(varn, 'ph')
                vol_idx = logical(op_tmp<tol_plume);
            else
                vol_idx = logical(op_tmp>tol_plume);
            end
            plume_val(idx2+j, k) = sum(volume_masked(vol_idx));
        end
    end

    idx2 = idx2 + size(output_tmp, 3);
    output_val(:,idx1:idx2) = reshape(output_tmp(logical(mask), :, :), ...
                                      sum(mask)*ntime, size(output_tmp, 3));

    idx1 = idx1 + size(output_tmp, 3);
end

%% Pod facilitated SBLgPCE
kl_size = 100;
nparam = numel(param_idx);
%%--------------Need to change -------------------------
tol_kl = 5e-2;
pool_size = 5;
%%--------------Need to change -------------------------

%%--------------Need to change -------------------------
pce_order = 4;
%%--------------Need to change -------------------------
trunc_rule = 'TD';
basis_type = 'legendre';
method = 'BCS';
diagnostic_level = 2;
monitor = 10;
% time_BCS = 1.e10;  %% infinitely large for BCS time tolerance
%%--------------Need to change -------------------------
tol_BCS = 1.0E-4;  % this number is of no use Need to set SB2_ControlSettings.m
%%--------------Need to change -------------------------
fixed_noise = false;
max_iter = 10000;

kl = karhunen_loeve(kl_size);
kl.construct(output_tra);
eig_err = (1-cumsum(max(0,kl.eigenvalues/kl.eig_total)));
idx_min_eig_err = find(eig_err < tol_kl);
M = min([idx_min_eig_err(1), kl_size, ns_tra]);
%%--------------Need to change -------------------------
M = 5;
%%--------------Need to change -------------------------
[xs_tra] = kl.project(output_tra, M, [], true);
[xs_val] = kl.project(output_val, M, [], true);

pp = parpool('cori_cluster', pool_size);

pce1 = pce_SBL_mo(nparam, M, pce_order, trunc_rule, basis_type, method);
pce1.set_SBL_options(SBL_path, fixed_noise, max_iter, tol_BCS, ...
                               diagnostic_level, monitor);
fprintf('Constructing PCE for mixing coefficients');
basis_train = pce1.construct(param_nlz_tra, xs_tra');
[xs_pred, basis_val] = pce1.pce_prediction([], param_nlz_val);
[err, err_norm] = pce1.compute_error(xs_val', xs_pred, ...
                                     true, 'rmse');
fprintf('Mean error for mixing coefficients is # %8.4f\n', mean(err, 1));


%% POD Reconstruction
output_pred = kl.basis(:, 1:M)*xs_pred' + repmat(kl.ref_basis, 1, ns_val);

%% POD validation
norm = sum(output_val.^2, 1);
norm(norm<1.e-6) = size(output_val, 2);
err_pod = sqrt(sum((output_pred-output_val).^2, 1)./norm);


plume_pred = zeros(ns_val, ntime);
for i = 1 : ns_val
    for j = 1 : ntime
        op_tmp = output_pred((j-1)*sum(mask)+1:j*sum(mask), i);
        if strcmp(varn, 'ph')
            vol_idx = logical(op_tmp<tol_plume);
        else
            vol_idx = logical(op_tmp>tol_plume);
        end
        plume_pred(i,j) = sum(volume_masked(vol_idx));
    end
end

corr = corrcoef(plume_val(:), plume_pred(:));

err_plume = zeros(ntime, 1);
for i = 1 : ntime
    nonzero_idx = logical(plume_val(:,i) ~= 0);
    err_plume(i) = sqrt(sum(((plume_pred(nonzero_idx,i)-plume_val(nonzero_idx,i))./...
        plume_val(nonzero_idx,i)).^2)/sum(nonzero_idx));
end

disp('Saving results...')
%%--------------Need to change -------------------------
% save(['/global/cscratch1/sd/yaning/Research/Liange_sim_new/PODfSBLgPCE_mo_' varn ...
%       '_results_tra1234val1po' num2str(pce_order) 'tol4nb' num2str(M)],...
%      'pce1', 'kl', 'xs_tra', 'xs_val', 'output_pred', 'output_val', 'plume_val', 'plume_pred', ...
%      'err','err_pod', 'corr', 'err_plume', 'volume_masked', 'volume', 'mask', '-v7.3');
save([data_path '/PODfSBLgPCE_mo_' varn ...
      '_results_tra1234val1po' num2str(pce_order) 'tol4nb' num2str(M)],...
     'pce1', 'kl', 'xs_tra', 'xs_val', 'output_pred', 'output_val', 'plume_val', 'plume_pred', ...
     'err','err_pod', 'corr', 'err_plume', 'volume_masked', 'volume', 'mask', '-v7.3');
%%--------------Need to change -------------------------

delete(gcp('nocreate'))
