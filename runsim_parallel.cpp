#include <iostream>
#include <mpi.h>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <unistd.h>

int main(int argc, char * argv[]) {
    MPI_Init(&argc, &argv);
    std::istringstream buffer(argv[1]);
    int nsample;
    buffer >> nsample;
    int ncpu, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &ncpu);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::vector<int> local_assignment;
    for (int i=0; i<nsample; i++) {
        if (rank == (i%ncpu)) {
            local_assignment.push_back(i);
        }
    }
    // std::cout << "Number of tasks for rank " << rank << " is "
    //           << local_assignment.size() << std::endl;

    for (int i=0; i<local_assignment.size(); i++) {
        std::stringstream ss;
        ss << "run" << local_assignment[i];
        chdir(ss.str().c_str());
        std::cout << "Job " << local_assignment[i] << " is now running on rank "
                  << rank << std::endl << std::flush;
        std::system("./tr2.087_eco2n_lnx");
        chdir("..");
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
}
