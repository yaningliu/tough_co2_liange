#!/usr/bin/python3

### read the concentration of ph, tds, ...
### and put them in a mat file called data_co2.mat,
### with each variable named ph, tds, ... of size (ne, ntime, ns)
import string
import sys
import os
from numpy import *
from math import pow
import shutil
import scipy.io as sio

########### need to change ###########
ne = 31320  ## number of grid cells/elements
ns = 1200  ## number of samples
ntime = 21 ## number of times
########### need to change ###########

## Find the runs that are successful. Store the numbers in set_of_success
iter_filen = 'iter.dat'
set_of_success = [];
for i in range(ns):
    os.chdir('run'+str(i))
    print('Checking run ' + str(i) + ' ...')
    if os.path.isfile(iter_filen) and os.stat(iter_filen).st_size>0:
        with open(iter_filen, 'r') as iter_if:
            lines_tmp = iter_if.readlines()
            if len(lines_tmp) >= 4 and len(lines_tmp[-4].split()) >= 2:
                if lines_tmp[-4].split()[1] == '0.199868E+03':
                    set_of_success.append(i)
    os.chdir('..')
print('Successful sets are found, ' + str(len(set_of_success)) + ' runs are successful')
sys.stdout.flush()

volume = empty([ne])
volume[:]=0.0
## mask are True and false, marks if a grid cell is at boundary 
mask = empty([ne])
mask[:] = True

ph = empty([ne, ntime, ns])
tds = empty([ne, ntime, ns])
ars = empty([ne, ntime, ns])
bar = empty([ne, ntime, ns])
cd = empty([ne, ntime, ns])
pb = empty([ne, ntime, ns])
ben = empty([ne, ntime, ns])
phe = empty([ne, ntime, ns])
nap = empty([ne, ntime, ns])

ph[:,:,:]=0.0
tds[:,:,:] = 0.0
ars[:,:,:] = 0.0
bar[:,:,:] = 0.0
cd[:,:,:]=0.0
pb[:,:,:]=0.0
ben[:,:,:]=0.0
phe[:,:,:]=0.0
nap[:,:,:]=0.0

# reading the volume data from MESH file
inf = open('MESH',"r")
inf.readline()                        # skip the first row

## There are many boundary mesh elements whose volumes are very large (>1E+55)
# for i in range(20):
#     inf.readline();                        # skip the first 20 element because it has large volume

for it in range(ne):
    inf.read(20)
    volume[it] = float(inf.read(10))
    if volume[it] > 1.0E+30:  # if volume is too large
        volume[it] = 0.0
        mask[it] = False
    inf.readline()
inf.close()

for it in range(ns):
    print('Processing run ' + str(it) + ' ...')
    b = str(it)
    c = "run"+b
    os.chdir(c)
    ny = 0
    time = empty([ntime])
    inf = open('hpip_conc.dat','r')
    for il in range(9):      #read the fist 9 line and the time zero in hpip_conc.dat and  the 'exchangeabble cations after time zero ( a bug in TOUGHREACT)
        inf.readline()

    for nt in range(ntime):          #now we have 21 time step
        line = inf.read(9)
        if len(line) == 0:
            break
        ny = ny + 1
        time[nt] = float(inf.read(12))
        inf.readline()
        for ie in range(ne):
            line = inf.readline()
            value = line.split()

            ph[ie, nt, it] = eval(value[8])
            # tds = Ca + Mg + Na + K + Cl + SO4 + HCO3
            # Transform tds from mol/l to mg/l here. For the others, no transform
            tds[ie, nt, it] = ( eval(value[10])*40078 + eval(value[11])*24305 +
                           eval(value[12])*22989.769 + eval(value[13])*39098.3 +
                           eval(value[14])*35453 + eval(value[15])*96062.6 +
                           eval(value[16])*61016.8 )
            ars[ie, nt, it] = eval(value[17])
            bar[ie, nt, it] = eval(value[18])
            cd[ie, nt, it] = eval(value[19])
            pb[ie, nt, it] = eval(value[20])
            ben[ie, nt, it] = eval(value[21])
            phe[ie, nt, it] = eval(value[22])
            nap[ie, nt, it] = eval(value[23])

    inf.close()
    os.chdir("..")

## data file name to write to for each output
save_fn = 'data_co2'

dic = {}
dic['ph'] = ph
dic['tds'] = tds
dic['ars'] = ars
dic['bar'] = bar
dic['cd'] = cd
dic['pb'] = pb
dic['ben'] = ben
dic['phe'] = phe
dic['nap'] = nap

dic['successful'] = set_of_success
# write the ph, plume value of every run for checking

sio.savemat(save_fn, dic)
