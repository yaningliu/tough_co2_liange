%% Implement POD facilitated GPR for Liange's problem
clear all;

%% set it to true if using matlab parallel feature; otherwise set it to false
if_parallel = false;

rom_path = '/global/homes/y/yaning/repos/rbf/rom_class';
addpath(genpath(rom_path));

data_path = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/';

ne = 31320;  % number of grid cells
ntime = 21; % number of times
param_idx = [1:8 10:31];  % one parameter is not used

data_tra_fn = 'data_co2_ph_all_sz2400_v1.mat';
data_val_fn = 'data_co2_ph_all_sz1200_v1.mat';

data_tra = load([data_path data_tra_fn]);
data_val = load([data_path data_val_fn]);

%% Note the local_assignment in the data is 0-based index. So +1 is needed.
param_tra = data_tra.parameters_mapped(data_tra.local_assignment+1, param_idx);
param_val = data_val.parameters_mapped(data_val.local_assignment+1, param_idx);

ns_tra = size(data_tra.ph, 3);  % number of training samples
ns_val = size(data_val.ph, 3);  % number of validation samples

output_tra = reshape(data_tra.ph(logical(data_tra.mask), :, :), sum(data_tra.mask)*ntime, ns_tra);
output_val = reshape(data_val.ph(logical(data_val.mask), :, :), sum(data_val.mask)*ntime, ns_val);

%% PODFGPR
kl_size = 100;
nparam = numel(param_idx);

kg = kl_gpr(kl_size, ns_tra);
kg.xlb = min([param_tra; param_val], [], 1)';
kg.xub = max([param_tra; param_val], [], 1)';

for i = 1 : kl_size
    kg.gpr{i} = gaussian_process(ns_tra);
    kg.gpr{i}.covfunc = {@covSEard};
    ncov = nparam + 1;
    kg.gpr{i}.hyp = struct('mean', 0, 'cov', ones(ncov, 1), 'lik', -1);
    kg.gpr{i}.lb_hyp = struct('mean', -5, 'cov', -5*ones(ncov, 1), 'lik', -8);
    kg.gpr{i}.ub_hyp = struct('mean', 5, 'cov', 5*ones(ncov, 1), 'lik', -1);
end

%% Start parallel constructing
if (if_parallel)
    pool_size = 32;
    % cluster = parcluster('local');
    % cluster.NumWorkers = pool_size;
    % saveAsProfile(cluster, 'cori_cluster');
    pp = parpool('cori_cluster', pool_size);
end

kg.construct(param_tra', output_tra);
err = kg.validate(param_val', output_val);

if (if_parallel)
    %% close parfor
    delete(gcp('nocreate'))
end