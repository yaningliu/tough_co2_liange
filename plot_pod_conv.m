%% plot the convergence of eigenvalues

clear all;
var = {'ph', 'bar', 'pb', 'phe'};
data_path = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/';
color = {'b', 'r'};
marker = {'o', 's'};
ls = {'-', ':'};
lw = 1.5;
markersize = 12;
fs = 20;

ntime = 21;
data = cell(length(var), 1);
for i = 1 : length(var)
    disp(['loading data of ' var{i}])
    data_fn = ['data_co2_' var{i} '_all_sz2400_v1.mat'];
    data{i} = load([data_path data_fn]);
    switch i
      case 1
        ns = size(data{i}.ph, 3);  % number of training samples
        data{i} = reshape(data{i}.ph(logical(data{i}.mask), :, :), sum(data{i}.mask)*ntime, ns);
      case 2
        ns = size(data{i}.bar, 3);  % number of training samples
        data{i} = reshape(data{i}.bar(logical(data{i}.mask), :, :), sum(data{i}.mask)*ntime, ns);
      case 3
        ns = size(data{i}.pb, 3);  % number of training samples
        data{i} = reshape(data{i}.pb(logical(data{i}.mask), :, :), sum(data{i}.mask)*ntime, ns);
      case 4
        ns = size(data{i}.phe, 3);  % number of training samples
        data{i} = reshape(data{i}.phe(logical(data{i}.mask), :, :), sum(data{i}.mask)*ntime, ns);
    end
end

for i = 1 : length(var)
    disp(['Dealing with ' var{i}])
    figure;
    kl_size = 50;
    kl = karhunen_loeve(kl_size);
    kl.construct(data{i});

    yyaxis left;
    plot(1:kl_size, kl.eigenvalues, 'Color', color{1}, 'Marker', marker{1}, ...
         'MarkerSize', markersize, 'LineStyle', ls{1}, 'LineWidth', lw)
    xlabel('Basis number $\bar{M}$', 'FontSize', fs, 'interpreter', 'Latex');
    ylabel('Eigenvalues', 'FontSize', fs);
    xlim([0, kl_size]);
    yyaxis right;
    plot(1:kl_size, 1-cumsum(kl.eigenvalues)/kl.eig_total, 'Color', color{2}, ...
         'Marker', marker{2}, 'MarkerSize', markersize, 'LineStyle', ls{2}, ...
         'LineWidth', lw)
    ylabel('$1-\sum_{i=1}^{\bar{M}}{\lambda}_i/\sum_{i=1}^N{\lambda}_i$', ...
           'FontSize', fs, 'interpreter', 'Latex');
    l = legend('Eigenvalues', 'Non-captured fraction');
    l.FontSize = fs;
end