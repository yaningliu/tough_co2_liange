#!/usr/bin/python3

import string
import sys
import os
from numpy import *
from math import pow
import shutil
import scipy.io as sio

########### need to change ###########
ne = 31320
ns = 1200
ntime = 21
########### need to change ###########

volume = empty([ne])
volume[:]=0.0

tvolph = empty([ns,ntime])
tvoltds = empty([ns,ntime])
tvolars = empty([ns,ntime])
tvolbar = empty([ns,ntime])
tvolcd = empty([ns,ntime])
tvolpb = empty([ns,ntime])
tvolben = empty([ns,ntime])
tvolphe = empty([ns,ntime])
tvolnap = empty([ns,ntime])

tvolph[:,:]=0.0
tvoltds[:,:] = 0.0
tvolars[:,:] = 0.0
tvolbar[:,:] = 0.0
tvolcd[:,:]=0.0
tvolpb[:,:]=0.0
tvolben[:,:]=0.0
tvolphe[:,:]=0.0
tvolnap[:,:]=0.0

# reading the volume data from MESH file
inf = open('MESH',"r")
inf.readline()                        # skip the first row

## There are many boundary mesh elements whose volumes are very large (>1E+55)
# for i in range(20):
#     inf.readline();                        # skip the first 20 element because it has large volume

for it in range(ne):
    inf.read(20)
    volume[it] = float(inf.read(10))
    if volume[it] > 1.0E+30:  # if volume is too large
        volume[it] = 0.0
    inf.readline()
inf.close()

ph = empty([ntime,ne])
tds = empty([ntime,ne])
ars = empty([ntime,ne])
bar = empty([ntime,ne])
cd = empty([ntime,ne])
pb = empty([ntime,ne])
ben = empty([ntime,ne])
phe = empty([ntime,ne])
nap = empty([ntime,ne])

for it in range(ns):
    print('Processing run ' + str(it) + ' ...')
    b = str(it)
    c = "run"+b
    os.chdir(c)
    ny = 0
    time = empty([ntime])
    inf = open('hpip_conc.dat','r')
    for il in range(9):      #read the fist 9 line and the time zero in hpip_conc.dat and  the 'exchangeabble cations after time zero ( a bug in TOUGHREACT)
        inf.readline()

    for nt in range(ntime):          #now we have 21 time step
        line = inf.read(9)
        if len(line) == 0:
            break
        ny = ny +1
        time[nt] = float(inf.read(12))
        inf.readline()
        for ie in range(ne):
            line = inf.readline()
            value = line.split()

            ph[nt,ie] = eval(value[8])
            # tds = Ca + Mg + Na + K + Cl + SO4 + HCO3
            # Transform tds from mol/l to mg/l here. For the others, no transform
            tds[nt,ie] = ( eval(value[10])*40078 + eval(value[11])*24305 +
                           eval(value[12])*22989.769 + eval(value[13])*39098.3 +
                           eval(value[14])*35453 + eval(value[15])*96062.6 +
                           eval(value[16])*61016.8 )
            ars[nt,ie] = eval(value[17])
            bar[nt,ie] = eval(value[18])
            cd[nt,ie] = eval(value[19])
            pb[nt,ie] = eval(value[20])
            ben[nt,ie] = eval(value[21])
            phe[nt,ie] = eval(value[22])
            nap[nt,ie] = eval(value[23])

            if ph[nt,ie] < 7.0:
                tvolph[it,nt] += volume[ie]
            if tds[nt,ie] > 1300:
                tvoltds[it,nt] += volume[ie]
            if ars[nt,ie] > 1.2413e-7:
                tvolars[it,nt] += volume[ie]
            if bar[nt,ie] > 1.019e-6:
                tvolbar[it,nt] += volume[ie]
            if cd[nt,ie] > 2.22e-9:
                tvolcd[it,nt] += volume[ie]
            if pb[nt,ie] > 3.04e-9:
                tvolpb[it,nt] += volume[ie]
            if ben[nt,ie] > 3.8406e-10:
                tvolben[it,nt] += volume[ie]
            if phe[nt,ie] > 3.19e-11:
                tvolphe[it,nt] += volume[ie]
            if nap[nt,ie] > 1.538e-9:
                tvolnap[it,nt] += volume[ie]

    inf.close()
    os.chdir("..")

# write the ph, plume value of every run for checking
inf=open('ph_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the pH plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolph[it,iny]),
    print >> inf
inf.close()

# write the tds, plume value of every run for checking
inf=open('tds_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the tds plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvoltds[it,iny]),
    print >> inf
inf.close()

# write the ars, plume value of every run for checking
inf=open('ars_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the ars plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolars[it,iny]),
    print >> inf
inf.close()

# write the bar, plume value of every run for checking
inf=open('bar_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the bar plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolbar[it,iny]),
    print >> inf
inf.close()

# write the cd, plume value of every run for checking
inf=open('cd_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the cd plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolcd[it,iny]),
    print >> inf
inf.close()

# write the pb, plume value of every run for checking
inf=open('pb_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the pb plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolpb[it,iny]),
    print >> inf
inf.close()

# write the ben, plume value of every run for checking
inf=open('ben_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the ben plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolben[it,iny]),
    print >> inf
inf.close()

# write the phe, plume value of every run for checking
inf=open('phe_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the phe plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolphe[it,iny]),
    print >> inf
inf.close()

# write the nap, plume value of every run for checking
inf=open('nap_plumedata',"w")
timeout = time[0:ny]
# write the time serial
inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
# write the nap plume for different time in every run
for it in range(ns):
    print >> inf,'  run','%8d'%it,
    for iny in range(ny):
        print >> inf, '%e' %(tvolnap[it,iny]),
    print >> inf
inf.close()

# For anova-HDMR
# Write the output. Create a simout variable of size nsample by noutput
simout = concatenate((tvolph, tvoltds), axis=1)
simout = concatenate((simout, tvolars), axis=1)
simout = concatenate((simout, tvolbar), axis=1)
simout = concatenate((simout, tvolcd), axis=1)
simout = concatenate((simout, tvolpb), axis=1)
simout = concatenate((simout, tvolben), axis=1)
simout = concatenate((simout, tvolphe), axis=1)
simout = concatenate((simout, tvolnap), axis=1)

output_fn = 'total_output.txt'
savetxt(output_fn, simout, fmt='%.12e', delimiter=' ')

# # cut hdmr parameters:
# cut_order = 2
# samp_order = 5
# nparam = 4
# noutput = 1
# iden = 'sim1d'
# pt_type = 'legl'
# ipl_type = 'lagr'
# param_filen = (iden + '_' + 'cut_hdmr_par_ind_pt_' + pt_type + '_ipl_' +
#                ipl_type + '_co' + str(cut_order) + '_so' + str(samp_order)
#                + '.mat')
# simout_filen = (iden + '_' + 'cut_hdmr_func_eval_ind_pt_' + pt_type + '_ipl_'
#                 + ipl_type + '_co' + str(cut_order) + '_so' + str(samp_order)
#                 + '.mat')
# param_data = sio.loadmat(param_filen)
# param_ind = param_data['par_ind']
# sio.savemat(simout_filen, {'simout': simout, 'par_ind': param_ind})
