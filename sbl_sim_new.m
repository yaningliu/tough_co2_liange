clear all

tic
%----------need to change---------------------------------------------------------------
% num_run_sz1200 = 1126; % number of successful runs
% num_run_sz2400 = 2210;
only_nonallzero = true; % only consider non-all-zero cases
param_train_fn = ['/global/cscratch1/sd/yaning/Research/Liange_sim_new/sz2400/' ...
                    'parameters_normalized.txt'];
param_val_fn = ['/global/cscratch1/sd/yaning/Research/Liange_sim_new/sz1200/' ...
                    'parameters_normalized.txt'];
wk_dir = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/SBL';
output_train_fn = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/sz2400/';
output_val_fn = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/sz1200/';
rom_class_path = '~/repos/rbf/rom_class';
% Just choose one output variable
vars = 'ph';
%---------------------------------------------------------------------------------------

wd_old = pwd;
cd(wk_dir)

% All the output variables are:
vars_all = {'ars', 'bar', 'ben', 'cd', 'nap', 'pb', 'ph', 'phe', 'tds'};
% the index of those input parameters that related to each output
% ind_ip = {[1:8, 10:12, 21:25], [1:8, 10:12, 20:25], [1:8, 10:12, 15, 21:25], ...
%           [1:8, 10:12, 16, 25], [1:8, 10:12, 14, 21:24], ...
%           [1:8, 10:12, 13, 21:24], [1:8, 10:12, 17, 26, 29], ...
%           [1:8, 10:12, 18, 27, 30], [1:8, 10:12, 19, 28, 31]};
ind_ip = {[1:8,10:31], [1:8,10:31], [1:8,10:31], [1:8,10:31], [1:8,10:31], ....
          [1:8,10:31], [1:8,10:31], [1:8,10:31], [1:8,10:31]};
% the index of vars in vars_all
vars_id = find(strcmp(vars_all, vars));

output_train = dlmread([output_train_fn vars '_plumedata'], '', 1, 2);
%%% output_val = dlmread([output_val_fn vars '_plumedata'], '', 1, 2);
output_val = output_train;
addpath(rom_class_path);


%% Sparse Baysian Learning PCE:
nparam = length(ind_ip{vars_id});
noutput = 1;
ind_op = 8 ; % considering the ind_op-th output
pce_order = 5;
trunc_rule = 'TD';
basis_type = 'legendre';
method = 'BCS';
diagnostic_level = 2;
monitor = 10;
tol = 1.0e-4;
fixed_noise = false;
max_iter = 10000;
%%--------------------------------

%% construct pce class
% for i = 1 : noutput
for i = ind_op : ind_op
    if only_nonallzero
        % Find the cases where outputs are not zeros
        tmp = output_train~=0;
        nonallzero_ind_train = find(tmp(:,ind_op));
        num_run_train = length(nonallzero_ind_train); % number of successful runs
    else
        num_run_train = size(output_train, 1); % number of successful runs
        nonallzero_ind_train = 1:num_run_train;
    end

    if only_nonallzero
        tmp = output_val~=0;
        nonallzero_ind_val = find(tmp(:,ind_op));
        num_run_val = length(nonallzero_ind_val); % number of successful runs
    else
        num_run_val = size(output_val, 1); % number of successful runs
        nonallzero_ind_val = 1:num_run_val;
    end

    %% the numbers of the runs (second column in the plume data file)
    %% the '+1' converts Python numbering to matlab numbering
    run_num_train = dlmread([output_train_fn vars '_plumedata'], '', ...
                            [1 1 size(output_train) 1])+1;
    %%% run_num_val = dlmread([output_val_fn vars '_plumedata'], '', ...
    %%%                          [1 1 size(output_val, 1) 1]) + 1;
	run_num_val = run_num_train;

    % load parameters
    param_train = dlmread(param_train_fn);
    param_train = param_train';
    param_train = param_train(run_num_train(nonallzero_ind_train), ind_ip{vars_id});
    %%% param_val = dlmread(param_val_fn);
    %%% param_val = param_val';
    %%% param_val = param_val(run_num_val(nonallzero_ind_val), ...
    %%%                       ind_ip{vars_id});
	param_val = param_train;

    sim_train = output_train;
    sim_train = sim_train(nonallzero_ind_train, :);
    %%% sim_val = output_val;
    %%% sim_val = sim_val(nonallzero_ind_val, :);
	sim_val = sim_train;

    nsample_train = size(nonallzero_ind_train,1);
    %%% nsample_val = size(nonallzero_ind_val,1);
	nsample_val = nsample_train;
    nsample_all = nsample_train+nsample_val;

    pce1{i} = pce(nparam, pce_order, trunc_rule, basis_type, method);
    pce1{i}.set_stopping_criterion(10000, 1.0e-3);
    fprintf('Constructing PCE for output # %5d\n', i);
    phi_train = pce1{i}.construct(param_train, sim_train(:, i));
    [sim_pred{i}, phi_val] = pce1{i}.pce_prediction([], param_val);
    [err{i}, err_norm{i}] = pce1{i}.compute_error(sim_val(:,i), sim_pred{i}, ...
                                                  true, 'rmse');
    fprintf('Mean relative error is # %8.4f\n', mean(err{i}));
end

cd(wd_old)

toc
