clear all

tic
%----------need to change-----------------------------------------------------------------------
num_run_sz1200 = 1126; % number of successful runs
num_run_sz2400 = 2210;
only_nonallzero = true; % only consider non-all-zero cases
param_filen_sz1200 = ['/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz1200/' ...
                    'parameters_normalized.txt'];
param_filen_sz2400 = ['/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz2400/' ...
                    'parameters_normalized.txt'];
%-----------------------------------------------------------------------------------------------

wd_old = pwd;
cd '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/anova_hdmr'

% All the output variables are:
% vars = {'ars', 'bar', 'ben', 'cd', 'nap', 'pb', 'ph', 'phe', 'tds'};

% Suppose just choose part of it:
vars = {'ph'};
if only_nonallzero
    if length(vars)>1
        error(['The number of output variables considered cannot be more than 1 when only non all zero ' ...
               'cases are considered!!!!!!!!!!!'])
    end
end


foldern = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz2400/';
output_sz2400 = [];
for i = 1 : length(vars)
    op_tmp = dlmread([foldern vars{i} '_plumedata'], '', 1, 2);
    output_sz2400 = [output_sz2400 op_tmp];
end
if only_nonallzero
    nonallzero_ind_sz2400 = find(any(output_sz2400, 2));
else
    nonallzero_ind_sz2400 = 1:num_run_sz2400;
end

foldern = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz1200/';
output_sz1200 = [];
for i = 1 : length(vars)
    op_tmp = dlmread([foldern vars{i} '_plumedata'], '', 1, 2);
    output_sz1200 = [output_sz1200 op_tmp];
end
if only_nonallzero
    nonallzero_ind_sz1200 = find(any(output_sz1200, 2));
else
    nonallzero_ind_sz1200 = 1:num_run_sz1200;
end

foldern = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz1200/';
%% the numbers of the runs (second column in the plume data file)
%% the '+1' converts Python numbering to matlab numbering
run_num_sz1200 = dlmread([foldern vars{1} '_plumedata'], '', [1 1 num_run_sz1200 1])+1;
foldern = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz2400/';
run_num_sz2400 = dlmread([foldern vars{1} '_plumedata'], '', [1 1 num_run_sz2400 1])+1;

% load parameters
param_train = dlmread(param_filen_sz2400);
param_train = param_train';
param_train = param_train(run_num_sz2400(nonallzero_ind_sz2400), [1:8 10:end]);
param_validate = dlmread(param_filen_sz1200);
param_validate = param_validate';
param_validate = param_validate(run_num_sz1200(nonallzero_ind_sz1200), [1:8 10:end]);

sim_validate = output_sz1200';
sim_validate = sim_validate(:, nonallzero_ind_sz1200);

save('sim_new_data_validate.mat', 'sim_validate', 'param_validate')

dlmwrite(['sim_new_anova_hdmr_par_mc_ns' num2str(size(nonallzero_ind_sz2400,1)) '.txt'], param_train, ' ')
dlmwrite(['sim_new_anova_hdmr_func_eval_mc_ns' num2str(size(nonallzero_ind_sz2400,1)) '.txt'], ...
         output_sz2400(nonallzero_ind_sz2400, :), ' ')


rom_class_path = '/Users/yaningliu/GoogleDrive/repos/rbf/rom_class';
addpath(rom_class_path);

anova_order = 2;
% max_order = [10 5 5];
max_order = [8 5];
nsample = size(nonallzero_ind_sz2400,1);
nparam = 30;
noutput = length(vars)*21;
data_type = 'mc';
basis_type = 'lege';
iden = 'sim_new';
optimized_val = true;
threshhold_on = true;
% threshhold_val = [5 5 5];
threshhold_val = [5 5];

%create class
eval([iden ['= anova_hdmr(anova_order, max_order, nsample, nparam, noutput, data_type, basis_type, ' ...
            'iden);']]);

%compute coefficients
eval([iden '.anova_hdmr_coefficients;']);

%set optimized
eval([iden '.set_optimized(' num2str(optimized_val) ');']);

%set threshhold
eval([iden '.set_threshhold(' num2str(threshhold_on) ',' mat2str(threshhold_val) ');']);

%compute optimal order
eval([iden '.get_optimal_order;']);

%compute the approximation for validation data
eval([iden '.approx_anova_hdmr;']);

%load approximation file
eval(['load(' iden '.anova_hdmr_approx_file_name);']);

%load validation data
eval(['load(' iden '.data_validate_file_name);']);

cd(wd_old)
toc