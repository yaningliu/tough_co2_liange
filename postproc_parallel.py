## post-process output data. Write ph_plumedata, tds_plumedata, ars_plumedata,
## bar_plumedata, cd_plumedata, pb_plumedata, ben_plumedata, phe_plumedata,
## ben_plumedata

import string
import sys
import os
from numpy import *
from math import pow
import shutil
import scipy.io as sio
from mpi4py import MPI

########### need to change ###########
ne = 31320
ns = 1200
ntime = 21
########### need to change ###########

## Find the runs that are successful. Store the numbers in set_of_success
iter_filen = 'iter.dat'
set_of_success = [];
for i in range(ns):
    os.chdir('run'+str(i))
    print('Checking run ' + str(i) + ' ...')
    if os.path.isfile(iter_filen) and os.stat(iter_filen).st_size>0:
        with open(iter_filen, 'r') as iter_if:
            lines_tmp = iter_if.readlines()
            if len(lines_tmp) >= 4 and len(lines_tmp[-4].split()) >= 2:
                if lines_tmp[-4].split()[1] == '0.199868E+03':
                    set_of_success.append(i)
    os.chdir('..')
print('Successful sets are found, ' + str(len(set_of_success)) + ' runs are successful')
sys.stdout.flush()

# reading the volume data from MESH file
volume = empty([ne])
volume[:]=0.0

inf = open('MESH',"r")
inf.readline()                        # skip the first row

## There are many boundary mesh elements whose volumes are very large (>1E+55)

for it in range(ne):
    inf.read(20)
    volume[it] = float(inf.read(10))
    if volume[it] > 1.0E+30:  # if volume is too large
        volume[it] = 0.0
    inf.readline()
inf.close()
print('Volumes are read')
sys.stdout.flush()

# element-wise plume volumn of each run
ph = empty([ntime,ne])
tds = empty([ntime,ne])
ars = empty([ntime,ne])
bar = empty([ntime,ne])
cd = empty([ntime,ne])
pb = empty([ntime,ne])
ben = empty([ntime,ne])
phe = empty([ntime,ne])
nap = empty([ntime,ne])

local_assignment = []
rank = MPI.COMM_WORLD.Get_rank()
nproc = MPI.COMM_WORLD.Get_size()
print('Number of processors = ' + str(nproc))

## Find the local assignment, i.e. the runs each processor will process
## Stored in local_assignment
for i in range(len(set_of_success)):
    if rank == i%nproc:
        local_assignment.append(set_of_success[i])
print('Local assignments are found')
print('rank ' + str(rank) + ' has ' + str(len(local_assignment)) + ' assignments')
sys.stdout.flush()

## total plume volume for the runs processed on each processor
volph = empty([len(local_assignment), ntime])
voltds = empty([len(local_assignment), ntime])
volars = empty([len(local_assignment), ntime])
volbar = empty([len(local_assignment), ntime])
volcd = empty([len(local_assignment), ntime])
volpb = empty([len(local_assignment), ntime])
volben = empty([len(local_assignment), ntime])
volphe = empty([len(local_assignment), ntime])
volnap = empty([len(local_assignment), ntime])

volph[:,:]=0.0
voltds[:,:] = 0.0
volars[:,:] = 0.0
volbar[:,:] = 0.0
volcd[:,:]=0.0
volpb[:,:]=0.0
volben[:,:]=0.0
volphe[:,:]=0.0
volnap[:,:]=0.0

for it in range(len(local_assignment)):
    print('rank ' + str(rank) + ' has ' + str(len(local_assignment)) + ' run to process')
    print('rank ' + str(rank) + ' is processing run ' + str(local_assignment[it]) + ' ...')
    sys.stdout.flush()

    b = str(local_assignment[it])
    c = "run"+b
    os.chdir(c)
    ny = 0
    time = empty([ntime])
    inf = open('hpip_conc.dat','r')
    for il in range(9):      #read the fist 9 line and the time zero in hpip_conc.dat and  the 'exchangeabble cations after time zero ( a bug in TOUGHREACT)
        inf.readline()

    for nt in range(ntime):          #now we have 21 time step
        line = inf.read(9)
        if len(line) == 0:
            break
        ny = ny +1
        time[nt] = float(inf.read(12))
        inf.readline()
        for ie in range(ne):
            line = inf.readline()
            value = line.split()

            ph[nt,ie] = eval(value[8])
            # tds = Ca + Mg + Na + K + Cl + SO4 + HCO3
            # Transform tds from mol/l to mg/l here. For the others, no transform
            tds[nt,ie] = ( eval(value[10])*40078 + eval(value[11])*24305 +
                           eval(value[12])*22989.769 + eval(value[13])*39098.3 +
                           eval(value[14])*35453 + eval(value[15])*96062.6 +
                           eval(value[16])*61016.8 )
            ars[nt,ie] = eval(value[17])
            bar[nt,ie] = eval(value[18])
            cd[nt,ie] = eval(value[19])
            pb[nt,ie] = eval(value[20])
            ben[nt,ie] = eval(value[21])
            phe[nt,ie] = eval(value[22])
            nap[nt,ie] = eval(value[23])

            if ph[nt,ie] < 7.0:
                volph[it, nt] += volume[ie]
            if tds[nt,ie] > 1300:
                voltds[it, nt] += volume[ie]
            if ars[nt,ie] > 1.2413e-7:
                volars[it, nt] += volume[ie]
            if bar[nt,ie] > 1.019e-6:
                volbar[it, nt] += volume[ie]
            if cd[nt,ie] > 2.22e-9:
                volcd[it, nt] += volume[ie]
            if pb[nt,ie] > 3.04e-9:
                volpb[it, nt] += volume[ie]
            if ben[nt,ie] > 3.8406e-10:
                volben[it, nt] += volume[ie]
            if phe[nt,ie] > 3.19e-11:
                volphe[it, nt] += volume[ie]
            if nap[nt,ie] > 1.538e-9:
                volnap[it, nt] += volume[ie]

    inf.close()
    os.chdir("..")

MPI.COMM_WORLD.Barrier()

## Send volph, voltds, ..., local_assignment from all the other processors to
## Proc 0 and concatenate with the ones on Proc 0
for i in range(1, nproc):
    if rank == i:
        if len(local_assignment):
            MPI.COMM_WORLD.send(volph, dest=0, tag=1)
            MPI.COMM_WORLD.send(voltds, dest=0, tag=2)
            MPI.COMM_WORLD.send(volars, dest=0, tag=3)
            MPI.COMM_WORLD.send(volbar, dest=0, tag=4)
            MPI.COMM_WORLD.send(volcd, dest=0, tag=5)
            MPI.COMM_WORLD.send(volpb, dest=0, tag=6)
            MPI.COMM_WORLD.send(volben, dest=0, tag=7)
            MPI.COMM_WORLD.send(volphe, dest=0, tag=8)
            MPI.COMM_WORLD.send(volnap, dest=0, tag=9)
            MPI.COMM_WORLD.send(local_assignment, dest=0, tag=10)
        else:
            continue

    if rank == 0:
        volph_temp = MPI.COMM_WORLD.recv(source=i, tag=1)
        voltds_temp = MPI.COMM_WORLD.recv(source=i, tag=2)
        volars_temp = MPI.COMM_WORLD.recv(source=i, tag=3)
        volbar_temp = MPI.COMM_WORLD.recv(source=i, tag=4)
        volcd_temp = MPI.COMM_WORLD.recv(source=i, tag=5)
        volpb_temp = MPI.COMM_WORLD.recv(source=i, tag=6)
        volben_temp = MPI.COMM_WORLD.recv(source=i, tag=7)
        volphe_temp = MPI.COMM_WORLD.recv(source=i, tag=8)
        volnap_temp = MPI.COMM_WORLD.recv(source=i, tag=9)
        la_temp = MPI.COMM_WORLD.recv(source=i, tag=10)

        volph = concatenate((volph, volph_temp), axis = 0)
        voltds = concatenate((voltds, voltds_temp), axis = 0)
        volars = concatenate((volars, volars_temp), axis = 0)
        volbar = concatenate((volbar, volbar_temp), axis = 0)
        volcd = concatenate((volcd, volcd_temp), axis = 0)
        volpb = concatenate((volpb, volpb_temp), axis = 0)
        volben = concatenate((volben, volben_temp), axis = 0)
        volphe = concatenate((volphe, volphe_temp), axis = 0)
        volnap = concatenate((volnap, volnap_temp), axis = 0)
        local_assignment = local_assignment+la_temp


if (rank == 0):
    # sorted run numbers
    la_sorted = sorted(local_assignment)
    # sorting index (permutation)
    sort_ind = [local_assignment.index(x) for x in la_sorted]
    volph = volph[sort_ind, :]
    voltds = voltds[sort_ind, :]
    volars = volars[sort_ind, :]
    volbar = volbar[sort_ind, :]
    volcd = volcd[sort_ind, :]
    volpb = volpb[sort_ind, :]
    volben = volben[sort_ind, :]
    volphe = volphe[sort_ind, :]
    volnap = volnap[sort_ind, :]

    tvolph = volph
    tvoltds = voltds
    tvolars = volars
    tvolbar = volbar
    tvolcd = volcd
    tvolpb = volpb
    tvolben = volben
    tvolphe = volphe
    tvolnap = volnap


    # write the ph, plume value of every run for checking
    inf=open('ph_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the pH plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolph[it,iny]),
        print >> inf
    inf.close()

    # write the tds, plume value of every run for checking
    inf=open('tds_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the tds plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvoltds[it,iny]),
        print >> inf
    inf.close()

    # write the ars, plume value of every run for checking
    inf=open('ars_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the ars plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolars[it,iny]),
        print >> inf
    inf.close()

    # write the bar, plume value of every run for checking
    inf=open('bar_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the bar plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolbar[it,iny]),
        print >> inf
    inf.close()

    # write the cd, plume value of every run for checking
    inf=open('cd_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the cd plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolcd[it,iny]),
        print >> inf
    inf.close()

    # write the pb, plume value of every run for checking
    inf=open('pb_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the pb plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolpb[it,iny]),
        print >> inf
    inf.close()

    # write the ben, plume value of every run for checking
    inf=open('ben_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the ben plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolben[it,iny]),
        print >> inf
    inf.close()

    # write the phe, plume value of every run for checking
    inf=open('phe_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the phe plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolphe[it,iny]),
        print >> inf
    inf.close()

    # write the nap, plume value of every run for checking
    inf=open('nap_plumedata',"w")
    timeout = time[0:ny]
    # write the time serial
    inf.write("time(yr) "+" ".join(["%e" % value for value in timeout])+"\n")
    # write the nap plume for different time in every run
    for it in range(len(la_sorted)):
        print >> inf,'  run','%8d'%la_sorted[it],
        for iny in range(ny):
            print >> inf, '%e' %(tvolnap[it,iny]),
        print >> inf
    inf.close()

    # For anova-HDMR
    # Write the output. Create a simout variable of size nsample by noutput
    # simout = concatenate((tvolph, tvoltds), axis=1)
    # simout = concatenate((simout, tvolars), axis=1)
    # simout = concatenate((simout, tvolbar), axis=1)
    # simout = concatenate((simout, tvolcd), axis=1)
    # simout = concatenate((simout, tvolpb), axis=1)
    # simout = concatenate((simout, tvolben), axis=1)
    # simout = concatenate((simout, tvolphe), axis=1)
    # simout = concatenate((simout, tvolnap), axis=1)
    #
    # output_fn = 'total_output.txt'
    # savetxt(output_fn, simout, fmt='%.12e', delimiter=' ')

# # cut hdmr parameters:
# cut_order = 2
# samp_order = 5
# nparam = 4
# noutput = 1
# iden = 'sim1d'
# pt_type = 'legl'
# ipl_type = 'lagr'
# param_filen = (iden + '_' + 'cut_hdmr_par_ind_pt_' + pt_type + '_ipl_' +
#                ipl_type + '_co' + str(cut_order) + '_so' + str(samp_order)
#                + '.mat')
# simout_filen = (iden + '_' + 'cut_hdmr_func_eval_ind_pt_' + pt_type + '_ipl_'
#                 + ipl_type + '_co' + str(cut_order) + '_so' + str(samp_order)
#                 + '.mat')
# param_data = sio.loadmat(param_filen)
# param_ind = param_data['par_ind']
# sio.savemat(simout_filen, {'simout': simout, 'par_ind': param_ind})
