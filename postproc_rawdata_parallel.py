## post-process output data. Write ph_plumedata, tds_plumedata, ars_plumedata,
## bar_plumedata, cd_plumedata, pb_plumedata, ben_plumedata, phe_plumedata,
## ben_plumedata

import string
import sys
import os
from numpy import *
from math import pow
import shutil
import scipy.io as sio
from mpi4py import MPI
import h5py
import hdf5storage as h5s
import glob

########### need to change ###########
ne = 31320
ns = 2400
ntime = 21
# varn = ['ph', 'tds', 'ars', 'bar', 'cd', 'pb', 'ben', 'phe', 'nap']
varn = ['ph']
var_idx = [8, [10, 11, 12, 13, 14, 15, 16], 17, 18, 19, 20, 21, 22, 23]
########### need to change ###########

rank = MPI.COMM_WORLD.Get_rank()
nproc = MPI.COMM_WORLD.Get_size()
if rank == 0:
    print('Number of processors = ' + str(nproc))

## Find the runs that are successful. Store the numbers in set_of_success
if rank == 0:
    iter_filen = 'iter.dat'
    set_of_success = [];
    for i in range(ns):
        os.chdir('run'+str(i))
        print('Checking run ' + str(i) + ' ...')
        if os.path.isfile(iter_filen) and os.stat(iter_filen).st_size>0:
            with open(iter_filen, 'r') as iter_if:
                lines_tmp = iter_if.readlines()
                if len(lines_tmp) >= 4 and len(lines_tmp[-4].split()) >= 2:
                    if lines_tmp[-4].split()[1] == '0.199868E+03':
                        set_of_success.append(i)
        os.chdir('..')
    print('Successful sets are found, ' + str(len(set_of_success)) + ' runs are successful')
    sys.stdout.flush()

    ## Send it to all the other processors
    for j in range(1, nproc):
        MPI.COMM_WORLD.send(set_of_success, dest=j, tag=0)

## Receive on the other processors
if rank != 0:
    set_of_success = MPI.COMM_WORLD.recv(source=0, tag=0)

# reading the volume data from MESH file
if rank == 0:
    volume = empty([ne])
    volume[:]=0.0
    ## mask are True and false, marks if a grid cell is at boundary
    mask = empty([ne])
    mask[:] = True

    inf = open('MESH',"r")
    inf.readline()                        # skip the first row

    ## There are many boundary mesh elements whose volumes are very large (>1E+55)

    for it in range(ne):
        inf.read(20)
        volume[it] = float(inf.read(10))
        if volume[it] > 1.0E+30:  # if volume is too large
            volume[it] = 0.0
            mask[it] = False
        inf.readline()
    inf.close()
    print('Volumes are read')
    sys.stdout.flush()

## Find the local assignment, i.e. the runs each processor will process
## Stored in local_assignment
local_assignment = []
for i in range(len(set_of_success)):
    if rank == i%nproc:
        local_assignment.append(set_of_success[i])
print('rank ' + str(rank) + ' has ' + str(len(local_assignment)) + ' run to process')

## local ph, tds, ..., values,  processed on each processor
for i in range(len(varn)):
    str1 = varn[i] + '_l=zeros([ne, ntime, len(local_assignment)])'
    exec str1

for it in range(len(local_assignment)):
    print('rank ' + str(rank) + ' is processing run ' + str(local_assignment[it]) + ' ...')
    sys.stdout.flush()

    b = str(local_assignment[it])
    c = "run"+b
    os.chdir(c)
    ny = 0
    time = empty([ntime])
    inf = open('hpip_conc.dat','r')
    for il in range(9):      #read the fist 9 line and the time zero in hpip_conc.dat and  the 'exchangeabble cations after time zero ( a bug in TOUGHREACT)
        inf.readline()

    for nt in range(ntime):          #now we have 21 time step
        line = inf.read(9)
        if len(line) == 0:
            break
        ny = ny +1
        time[nt] = float(inf.read(12))
        inf.readline()
        for ie in range(ne):
            line = inf.readline()
            value = line.split()

            if 'ph' in varn:
                ph_l[ie, nt, it] = eval(value[8])
            if 'tds' in varn:
                tds_l[ie, nt, it] = ( eval(value[10])*40078 +
                                      eval(value[11])*24305 +
                                      eval(value[12])*22989.769 +
                                      eval(value[13])*39098.3 +
                                      eval(value[14])*35453 +
                                      eval(value[15])*96062.6 +
                                      eval(value[16])*61016.8 )
            if 'ars' in varn:
                ars_l[ie, nt, it] = eval(value[17])
            if 'bar' in varn:
                bar_l[ie, nt, it] = eval(value[18])
            if 'cd' in varn:
                cd_l[ie, nt, it] = eval(value[19])
            if 'pb' in varn:
                pb_l[ie, nt, it] = eval(value[20])
            if 'ben' in varn:
                ben_l[ie, nt, it] = eval(value[21])
            if 'phe' in varn:
                phe_l[ie, nt, it] = eval(value[22])
            if 'nap' in varn:
                nap_l[ie, nt, it] = eval(value[23])

    inf.close()
    os.chdir("..")
    print('rank ' + str(rank) + ' has finished processing run ' + str(local_assignment[it]) + ' ...')
    print('rank ' + str(rank) + ' has ' + str(len(local_assignment)-1-it) + ' more to finish')
    sys.stdout.flush()

MPI.COMM_WORLD.Barrier()

if rank == 0:
    print('All ranks have finished processing data')
    sys.stdout.flush()

la_allgather = MPI.COMM_WORLD.allgather(local_assignment)

## save concentration data
mat_fn1 = 'data_co2_run'+str(rank)
dic = {}
for i in range(len(varn)):
    str1 = "dic['" + varn[i] + "'] = " + varn[i] + '_l'
    exec str1
if len(local_assignment):
    h5s.savemat(mat_fn1, dic)

MPI.COMM_WORLD.Barrier()

## save gathered local assignment index and mask
if rank == 0:
    # make la_allgather a 1d array
    la_array = []
    for i in range(len(la_allgather)):
        la_array += la_allgather[i]
    la_array = array(la_array)

    # parameters
    par_n = 'parameters_normalized.txt'
    par_m = 'parameters_mapped.txt'
    parameters_nor = loadtxt(par_n).T
    parameters_mapped = loadtxt(par_m).T

    mat_fn2 = 'la_mask_par'
    dic = {}
    dic['local_assignment'] = la_array
    dic['mask'] = array(mask)
    dic['parameters_normalized'] = parameters_nor
    dic['parameters_mapped'] = parameters_mapped
    h5s.savemat(mat_fn2, dic)
    print('Data have been saved to ' + mat_fn1 + ' and ' + mat_fn2)

## Now load all the data written by all the processors and
## combine them in a single file
# Delete unuseful concentration data
for i in range(len(varn)):
    str1 = 'del '+ varn[i] + '_l'
    exec str1

for iv in range(len(varn)):
    if rank == iv:
        datasetn1 = 'la_mask_par.mat'
        lmp = h5s.loadmat(datasetn1)
        nss = lmp['local_assignment'].size  ## number of successful runs

        conc_data_all = zeros([ne, ntime, nss])
        pointer = 0
        for i in range(nproc):
            print('Rank ' + str(rank) + ': reading ' + varn[iv] + ' data from the ' + str(i) + '-th file')
            data_fn = 'data_co2_run'+str(i)+'.mat'
            if os.path.isfile(data_fn):
                data_tmp = h5s.loadmat(data_fn)
                ns_tmp = data_tmp[varn[iv]].shape[2]
                conc_data_all[:,:,pointer:pointer+ns_tmp] = data_tmp[varn[iv]]
                pointer = pointer+ns_tmp
        print('Rank ' + str(rank) + ':Data has been read from all files of ' + varn[iv])

        ## Save to mat file
        dic = {}
        dic[varn[iv]] = conc_data_all
        dic['local_assignment'] = lmp['local_assignment']
        dic['mask'] = lmp['mask']
        dic['parameters_normalized'] = lmp['parameters_normalized']
        dic['parameters_mapped'] = lmp['parameters_mapped']
        filen = 'data_co2_' + varn[iv] + '_all.mat'
        print('Rank ' + str(rank) + ':Saving overall data for ' + varn[iv])
        h5s.savemat(filen, dic)
        print('Rank ' + str(rank) + ':All data for ' + varn[iv] + 'have been saved')

MPI.COMM_WORLD.Barrier()
if rank == 0:
    print('All data for all outputs have been saved!')
    print('Now removing unuseful data')
    os.remove('la_mask_par.mat')
    filelist = glob.glob('data_co2_run*')
    for f in filelist:
        os.remove(f)
    print('All unuseful data have been removed! Everything finished! Bye!')
