import numpy as np
import scipy.io as sio
import os

conc_val_data = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/hpip_conc_tra836.dat'
conc_pred_data = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/hpip_conc_tra836_pred.dat'

ph_pred_val = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/ph_pred_sel.mat'

ph = sio.loadmat(ph_pred_val)
ph_pred = ph['ph_pred_sel']
mask = ph['mask']
print(ph_pred.shape)
print(mask.shape)

infile = open(conc_val_data,"r")
lines = infile.readlines()
infile.close()

lines_copy = lines
line_ptr = 0
for i in range(219257, 250577):
    if mask[0][i-219257] == 1:
        tmp = lines[i].split()
        tmp[8] = "%5.3f" % ph_pred[line_ptr]
        lines_copy[i] = '   '.join(tmp) + '\n'
        line_ptr += 1

with open(conc_pred_data, 'w') as file:
    file.writelines(lines_copy)
