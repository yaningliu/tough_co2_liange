import matplotlib.pyplot as plt
import os
import numpy as np

cur_dir = os.getcwd()
folder_name = '/clusterfs/lawrencium/yaningl/sim_new/sim_new_011716'
num_sim = 2400
iter_filen = 'iter.dat'
flow_filen = 'flow.inp'
set_of_success = [];
co2_rate_success = [];
co2_rate_notsuccess = [];

os.chdir(folder_name)
for i in range(num_sim):
    os.chdir('run'+str(i))
    with open(iter_filen, 'r') as iter_if:
        if iter_if.readlines()[-4].split()[1] == '0.199868E+03':
            set_of_success.append(i)
            # read co2ratem
            with open(flow_filen) as flow_if:
                co2_rate_success.append(float(flow_if.readlines()[-8].split()[1]))
        else:
            # read co2ratem
            with open(flow_filen) as flow_if:
                co2_rate_notsuccess.append(float(flow_if.readlines()[-8].split()[1]))
    os.chdir('..')

os.chdir(cur_dir)
np.savetxt('co2_rate_suc.txt', np.array(co2_rate_success))
np.savetxt('co2_rate_nsuc.txt', np.array(co2_rate_notsuccess))

# # plot co2 rate
# xvals_suc = range(1, len(set_of_success)+1)
# xvals_nsuc = range(len(set_of_success)+1, num_sim+1)
# plt.plot(xvals_suc, list(np.log10(co2_rate_success)), 'ro', xvals_nsuc, list(np.log10(co2_rate_notsuccess)), 'b^')
# plt.savefig('co2_rate_plot', format='png')
# plt.close()
