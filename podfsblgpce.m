%% Implement POD facilitated GPR for Liange's problem
clear all;

%% set it to true if using matlab parallel feature; otherwise set it to false
if_parallel = false;

rom_path = '/global/homes/y/yaning/repos/rbf/rom_class';
SBL_path = '/global/homes/y/yaning/repos/rbf/SB2_Release_200';
addpath(genpath(rom_path));
addpath(SBL_path);

data_path = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/';

ne = 31320;  % number of grid cells
ntime = 21; % number of times
param_idx = [1:8 10:31];  % one parameter is not used

data_tra_fn = 'data_co2_ph_all_sz2400_v1.mat';
data_val_fn = 'data_co2_ph_all_sz1200_v1.mat';

data_tra = load([data_path data_tra_fn]);
data_val = load([data_path data_val_fn]);

%% Note the local_assignment in the data is 0-based index. So +1 is needed.
param_tra = data_tra.parameters_mapped(data_tra.local_assignment+1, param_idx);
param_val = data_val.parameters_mapped(data_val.local_assignment+1, param_idx);
param_nlz_tra = data_tra.parameters_normalized(data_tra.local_assignment+1, param_idx);
param_nlz_val = data_val.parameters_normalized(data_val.local_assignment+1, param_idx);

ns_tra = size(data_tra.ph, 3);  % number of training samples
ns_val = size(data_val.ph, 3);  % number of validation samples

output_tra = reshape(data_tra.ph(logical(data_tra.mask), :, :), sum(data_tra.mask)*ntime, ns_tra);
output_val = reshape(data_val.ph(logical(data_val.mask), :, :), sum(data_val.mask)*ntime, ns_val);

clear data_tra data_val

%% Pod facilitated SBLgPCE
kl_size = 100;
nparam = numel(param_idx);
tol_kl = 6e-2;
pool_size = 32;

pce_order = 4;
trunc_rule = 'TD';
basis_type = 'legendre';
method = 'BCS';
diagnostic_level = 2;
monitor = 10;
tol_BCS = 2.0E-2;
fixed_noise = false;
max_iter = 10000;

kl = karhunen_loeve(kl_size);
kl.construct(output_tra);
eig_err = (1-cumsum(max(0,kl.eigenvalues/kl.eig_total)));
idx_min_eig_err = find(eig_err < tol_kl);
M = min([idx_min_eig_err(1), kl_size, ns_tra]);
[xs_tra] = kl.project(output_tra, M, [], true);
[xs_val] = kl.project(output_val, M, [], true);

% pp = parpool('cori_cluster', pool_size);

pce1 = cell(M, 1);
xs_pred = cell(M, 1);
err = cell(M, 1);
err_norm = cell(M, 1);
parfor i = 1 : M
    pce1{i} = pce_SBL(nparam, pce_order, trunc_rule, basis_type, method);
    pce1{i}.set_SBL_options(SBL_path, fixed_noise, max_iter, tol_BCS, ...
                                      diagnostic_level, monitor);
    fprintf('Constructing PCE for mixing coefficients # %5d\n', i);
    basis_train = pce1{i}.construct(param_nlz_tra, xs_tra(i,:)');
    [xs_pred{i}, basis_val] = pce1{i}.pce_prediction([], param_nlz_val);
    [err{i}, err_norm{i}] = pce1{i}.compute_error(xs_val(i,:)', xs_pred{i}, ...
                                                  true, 'rmse');
    fprintf('Mean error for mixing coefficients is # %8.4f\n', mean(err{i}, 1));
end

%% POD Reconstruction
xs_pred_mat = zeros(M, ns_val);
for i = 1 : M
    xs_pred_mat(i, :) = xs_pred{i};
end
output_pred = kl.basis(:, 1:M)*xs_pred_mat + repmat(kl.ref_bases, 1, ns_val);

%% POD validation
norm = sum(output_val.^2, 1);
norm(norm<1.e-6) = size(output_val, 2);
err = sqrt(sum((output_pred-output_val).^2, 1)./norm);

%% Compute plume volume
% tol_plume = 7.0;
% plume_vol_val = np.zeros(ns_val, ntime);
% plume_vol_pred = np.zeros(ns_val, ntime);
% for i = 1 : ntime:
%     ph_tmp = data_tra.ph(logical(data_tra.mask), i, :);
%     vol_tmp =
%     plume_vol_val = sum(ph_tmp(ph_tmp>tol_plume))
% plume_vol = data_tra.ph(logical(data_tra.mask), :, :)

save PODfSBLgPCE_results pce1 kl xs_tra xs_val output_pred

% delete(gcp('nocreate'))
