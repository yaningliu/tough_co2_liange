"""Use xgboost for Liange's problem
Time-stamp: <2016-08-02 13:30:15 yaning>

Author: Yaning Liu
"""

import os
import numpy as np
import h5py
import time
import xgboost as xgb
from mpi4py import MPI

comm = MPI.COMM_WORLD

rank = comm.Get_rank()
nproc = comm.Get_size()

if rank == 0:
    t0 = time.time()

# -------------Need to change-------------------
machine = 'Cori'  # 'Mac' or 'Cori'

if machine == 'Cori':
    prefix = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/'
elif machine == 'Lrc':
    prefix = '/clusterfs/lawrencium/yaningl/Research/sim_new/'

only_nonallzero = True; # only consider non-all-zero cases
# param_train_fn = prefix + 'sz2400/parameters_normalized.txt'
# param_val_fn = prefix + 'sz1200/parameters_normalized.txt'
param_train_fn = prefix + 'sz2400/parameters_mapped.txt'
param_val_fn = prefix + 'sz1200/parameters_mapped.txt'
results_fn = prefix + 'results.hdf5'
wk_dir = prefix + 'python_results'
output_train_fn = prefix + 'sz2400/'
output_val_fn = prefix + 'sz1200/'
# rom_class_path = '/Users/yaningliu/GoogleDrive/repos/rbf/rom_class'
# Just choose one output variable
ne = 31320
ntime = 21
varn = 'ph'
tol_plume = 7.0
# tol_plume = 3.04e-9;  # for pb
# tol_plume = 3.19e-11;  # for phe
# tol_plume = 1.019e-6;  # for bar

xgb_n_est = 300
xgb_depth = 3
# -----------------------------------------------------------------------------

# All the output variables are:
vars_all = ['ars', 'bar', 'ben', 'cd', 'nap', 'pb', 'ph', 'phe', 'tds']
# the index of those input parameters that related to each output
# ind_ip = [np.concatenate((range(0, 8), range(9, 13), range(19, 26))),
#           np.concatenate((range(0, 8), range(9, 13), range(19, 26))),
#           np.concatenate((range(0, 8), range(9, 13), [14], range(20, 26))),
#           np.concatenate((range(0, 8), range(9, 13), [15], [24])),
#           np.concatenate((range(0, 8), range(9, 13), [13], range(20, 25))),
#           np.concatenate((range(0, 8), range(9, 13), [12], range(20, 25))),
#           np.concatenate((range(0, 8), range(9, 13), [16], [25], [28])),
#           np.concatenate((range(0, 8), range(9, 13), [17], [26], [29])),
#           np.concatenate((range(0, 8), range(9, 13), [18], [27], [30]))]

ind_ip = [np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31))),
          np.concatenate((range(0, 8), range(9, 31)))]

# the index of varn in vars_all
vars_id = vars_all.index(varn)
nparam = ind_ip[vars_id].size

# data_tra_fn = ['data_co2_' + varn + '_all_sz2400_v1.mat',
#                'data_co2_' + varn + '_all_sz2400_v3.mat',
#                'data_co2_' + varn + '_all_sz2400_v4.mat']
data_tra_fn = ['data_co2_' + varn + '_all_sz2400_v1.mat']
data_val_fn = ['data_co2_' + varn + '_all_sz1200_v1.mat']

with h5py.File(prefix+data_tra_fn[0], 'r') as fh:
    mask = fh['mask'][()]
    mask = mask.flatten()
    volume_all = fh['volume'][()]
    volume_all = volume_all.flatten()
    volume_masked = volume_all[mask.astype(bool)]

param_norm_tra = np.empty([0, nparam], dtype=float)
param_mapped_tra = np.empty([0, nparam], dtype=float)
# training input
for i in range(len(data_tra_fn)):
    with h5py.File(prefix+data_tra_fn[i], 'r') as fh:
        la = fh['local_assignment'][()]
        param_tmp = fh['parameters_normalized'][()]
        param_tmp = param_tmp.transpose()
        param_norm_tra = np.concatenate(
            (param_norm_tra, param_tmp[la, ind_ip[vars_id]]), axis=0)
        param_tmp = fh['parameters_mapped'][()]
        param_tmp = param_tmp.transpose()
        param_mapped_tra = np.concatenate(
            (param_mapped_tra, param_tmp[la, ind_ip[vars_id]]), axis=0)
ns_tra = param_norm_tra.shape[0]

param_norm_val = np.empty([0, nparam])
param_mapped_val = np.empty([0, nparam])
# validation data
for i in range(len(data_val_fn)):
    with h5py.File(prefix+data_val_fn[i], 'r') as fh:
        la = fh['local_assignment'][()]
        param_tmp = fh['parameters_normalized'][()]
        param_tmp = param_tmp.transpose()
        param_norm_val = np.concatenate(
            (param_norm_val, param_tmp[la, ind_ip[vars_id]]), axis=0)
        param_tmp = fh['parameters_mapped'][()]
        param_tmp = param_tmp.transpose()
        param_mapped_val = np.concatenate(
            (param_mapped_val, param_tmp[la, ind_ip[vars_id]]), axis=0)
ns_val = param_norm_val.shape[0]

dof = np.sum(mask.astype(int))*ntime
dof1 = np.sum(mask.astype(int))

if rank == 0:
    output_tra = np.zeros((dof, ns_tra))
    output_val = np.zeros((dof, ns_val))

    plume_tra = np.zeros((ns_tra, ntime))
    idx1 = 0
    idx2 = 0
    for i in range(len(data_tra_fn)):
        with h5py.File(prefix+data_tra_fn[i], 'r') as fh:
            output_tmp = fh[varn][()]
            output_tmp = np.transpose(output_tmp, (2,1,0))

        for j in range(output_tmp.shape[2]):
            for k in range(ntime):
                op_tmp = output_tmp[mask.astype(bool), k, j]
                if varn == 'ph':
                    vol_idx = np.where(op_tmp < tol_plume)
                else:
                    vol_idx = np.where(op_tmp > tol_plume)
                plume_tra[idx2+j, k] = np.sum(volume_masked[vol_idx])

        idx2 = idx2 + output_tmp.shape[2]
        output_tra[:, idx1:idx2] = np.reshape(output_tmp[mask.astype(bool), :, :],
                                              (dof,
                                               output_tmp.shape[2]), order='F')
        idx1 = idx1 + output_tmp.shape[2]
    output_tra = np.transpose(output_tra)
    print('Rank{}: Training output is obtained'.format(rank), flush=True)

    plume_val = np.zeros((ns_val, ntime))
    idx1 = 0
    idx2 = 0
    for i in range(len(data_val_fn)):
        with h5py.File(prefix+data_val_fn[i], 'r') as fh:
            output_tmp = fh[varn][()]
            output_tmp = np.transpose(output_tmp, (2,1,0))

        for j in range(output_tmp.shape[2]):
            for k in range(ntime):
                op_tmp = output_tmp[mask.astype(bool), k, j]
                if varn == 'ph':
                    vol_idx = np.where(op_tmp < tol_plume)
                else:
                    vol_idx = np.where(op_tmp > tol_plume)
                plume_val[idx2+j, k] = np.sum(volume_masked[vol_idx])

        idx2 = idx2 + output_tmp.shape[2]
        output_val[:, idx1:idx2] = np.reshape(output_tmp[mask.astype(bool), :, :],
                                              (dof, output_tmp.shape[2]),
                                              order='F')
        idx1 = idx1 + output_tmp.shape[2]
    output_val = np.transpose(output_val)
    print('Rank {}: Validation output is obtained'.format(rank), flush=True)

    # output_train = np.loadtxt(output_train_fn+varn+'_plumedata',
    #                           skiprows=1, usecols=range(2, 23))
    # output_val = np.loadtxt(output_val_fn+varn+'_plumedata',
    #                         skiprows=1, usecols=range(2, 23))

# Distribute work:
local_assignment = list(range(rank*dof//nproc,
                              (rank+1)*dof//nproc))
print('Rank {}: local assignment is {}:{}'.format(rank, local_assignment[0],
                                                  local_assignment[-1]),
      flush=True)
output_tra_local = np.zeros((ns_tra, len(local_assignment)))

sim_pred_local = np.zeros((ns_val, len(local_assignment)))

if rank == 0:
    sim_pred = np.zeros((ns_val, dof))
    max_rel_err = np.zeros(dof,)
    mean_rel_err = np.zeros(dof,)
    rrmse = np.zeros(dof,)
    plume_pred = np.zeros((ns_val, ntime))

    output_tra_local = output_tra[:, 0:dof//nproc]

    print('Rank {}: Sending data to the other ranks!'.format(rank),
                  flush=True)
    for i in range(1, nproc):
        comm.Send(output_tra[:, i*dof//nproc:(i+1)*dof//nproc,], dest=i)

if rank != 0:
    print('Rank {}: Receiving data sent by Rank 0!'.format(rank),
          flush=True)
    comm.Recv(output_tra_local, source=0)
    print('Rank {}: Output data have been received from Rank 0! '
          'The size of the data is {} by {}'.format(
              rank, output_tra_local.shape[0], output_tra_local.shape[1]),
          flush=True)

for la in local_assignment:
    print('Rank {}: la = {}'.format(rank, la), flush=True)
    if la-local_assignment[0] % 100 == 0:
        print('Rank{}: {} XGBoost finished out of {}'
              .format(rank, la-local_assignment[0], len(local_assignment)),
              flush=True)
    xgb_model = xgb.XGBRegressor(max_depth=xgb_depth, n_estimators=xgb_n_est)
    xgb_model.fit(param_mapped_tra, output_tra_local[:, la-local_assignment[0]])
    sim_pred_local[:, la-local_assignment[0]] = xgb_model.predict(param_mapped_val)

print('Rank {}: XGBoost finished!'.format(rank), flush=True)

comm.Barrier()


if rank != 0:
    comm.Send(sim_pred_local, dest=0)

if rank == 0:
    sim_pred[:, local_assignment] = sim_pred_local

    for i in range(1, nproc):
        sim_tmp = np.zeros((ns_val, (i+1)*dof//nproc-i*dof//nproc))
        comm.Recv(sim_tmp, source=i)
        sim_pred[:, i*dof//nproc:(i+1)*dof//nproc] = sim_tmp

    print('Rank {}: All predictions from other ranks have been received!'
          .format(rank), flush=True)

if rank == 0:

    for i in range(dof):
        max_rel_err[i] = np.max(np.fabs(sim_pred[:, i] - output_val[:, i])/output_val[:, i])
        mean_rel_err[i] = np.mean(np.fabs(sim_pred[:, i] - output_val[:, i])/output_val[:, i])
        rrmse[i] = np.sqrt(np.sum(np.square(np.fabs(sim_pred[:, i] - output_val[:, i])/output_val[:, i]))/ns_val)
        if i%100 == 0:
            print('Rank {}: XGBoost is dealing with output {} out of total {} outputs'
                  .format(rank, i, dof), flush=True)
            print('Rank {}: max_rel_err is {}'.format(rank, max_rel_err[i]),
                  flush=True)
            print('Rank {}: mean_rel_err is {}'.format(rank, mean_rel_err[ i]),
                  flush=True)
            print('Rank {}: relative rmse is {}'.format(rank, rrmse[i]),
                  flush=True)


    for i in range(ns_val):
        for j in range(ntime):
            #----------------- Need to change > or <  --------------
            plume_pred[i, j] = np.sum(
                volume_masked[np.where(sim_pred[i, j*dof1:(j+1)*dof1]
                                       > tol_plume)[0]])

    wd_old = os.getcwd()
    os.chdir(wk_dir)

    with h5py.File(results_fn, 'w') as fh:
        fh.create_dataset('sim_pred', data = sim_pred)
        fh.create_dataset('plume_pred', data = plume_pred)
        fh.create_dataset('max_rel_err', data = max_rel_err)
        fh.create_dataset('mean_rel_err', data = mean_rel_err)
        fh.create_dataset('rrmse', data = rrmse)
        fh.create_dataset('plume_pred', data = plume_pred)
        fh.create_dataset('output_val', data = output_val)
        fh.create_dataset('plume_val', data = plume_val)

    os.chdir(wd_old)
    print('Rank {}: Time elapsed: {} seconds'.format(rank, time.time() - t0))
