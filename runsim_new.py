#!/usr/bin/python

import string
import sys
import os
from numpy import *
from math import pow
import shutil
import subprocess

# begin to run the models
nrun = sys.argv[1]

for it in range(0, nrun):
  b = str(it)
  c = "run"+b
  os.chdir(c)
  subprocess.call(["./tr2.087_eco2n_lnx"])
  os.chdir("..")
