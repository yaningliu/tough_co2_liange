%% plot the convergence of eigenvalues

clear all;
var = {'ph', 'bar', 'pb', 'phe'};
data_path = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/';
filen = {'PODfSBLgPCE_mo_ph_results_tra1val1po4tol4nb20.mat', ...
         'PODfSBLgPCE_mo_bar_results_tra1val1po4tol4nb20.mat', ...
         'PODfSBLgPCE_mo_pb_results_tra1val1po4tol4nb20.mat', ...
         'PODfSBLgPCE_mo_phe_results_tra1val1po4tol4nb20.mat'};
color = {'b', 'r'};
marker = {'o', 's'};
ls = {'-', ':'};
lw = 1.5;
markersize = 12;
fs = 20;

data = cell(length(var), 1);
for i = 1 : length(var)
    disp(['loading data of ' var{i}])
    data{i} = load([data_path filen{i}], 'pce1');
end

for i = 1 : length(var)
    disp(['Dealing with ' var{i}])
    figure;

    sparsity = zeros(data{i}.pce1.noutput, 1);
    num_nonzero = zeros(data{i}.pce1.noutput, 1);
    for j = 1 : data{i}.pce1.noutput
        N_pc = data{i}.pce1.ncoef;
        sparsity(j) = (N_pc - numel(data{i}.pce1.indset{j}))/N_pc;
        num_nonzero(j) = numel(data{i}.pce1.indset{j});
    end

    sparsity_sorted = sort(sparsity);
    num_nonzero_sorted = sort(num_nonzero);

    fs = 12;
    yyaxis left
    plot(1:numel(sparsity_sorted), sparsity_sorted, 'o');
    set(gca, 'FontSize', fs)
    ylabel('Sparsity of SBL')
    xlabel('POD coefficient number', 'FontSize', fs)

    darkgreen = [0,153,0]/255;
    color = {'r', 'b', darkgreen};
    yyaxis right
    plot(1:numel(sparsity_sorted), num_nonzero_sorted, 'x');
    ylabel('Number of nonzero terms')
    set(gca, 'FontSize', fs)
end