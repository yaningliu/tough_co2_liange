import matplotlib.pyplot as plt
import os
import sys
import numpy as np
import time
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model
import xgboost as xgb

t0 = time.time()

# -------------Need to change-------------------

only_nonallzero = True; # only consider non-all-zero cases
prefix = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/'
# param_train_fn = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz2400/parameters_normalized.txt'
# param_val_fn = '/Users/yaningliu/GoogleDrive/Research/Liange/Liange_sim_new/sz1200/parameters_normalized.txt'
param_train_fn = prefix + 'sz2400/parameters_mapped.txt'
param_val_fn = prefix + 'sz1200/parameters_mapped.txt'
wk_dir = prefix + 'python_results'
output_train_fn = prefix + 'sz2400/'
output_val_fn = prefix + 'sz1200/'
rom_class_path = '/Users/yaningliu/GoogleDrive/repos/rbf/rom_class'
# Just choose one output variable
vars = 'ph'
# -----------------------------------------------------------------------------

wd_old = os.getcwd()
os.chdir(wk_dir)

# All the output variables are:
vars_all = ['ars', 'bar', 'ben', 'cd', 'nap', 'pb', 'ph', 'phe', 'tds']
# the index of those input parameters that related to each output
ind_ip = [np.concatenate((range(1, 9), range(10, 13), range(21, 26))),
          np.concatenate((range(1, 9), range(10, 13), range(20, 26))),
          np.concatenate((range(1, 9), range(10, 13), [15], range(21, 26))),
          np.concatenate((range(1, 9), range(10, 13), [16], [25])),
          np.concatenate((range(1, 9), range(10, 13), [14], range(21, 25))),
          np.concatenate((range(1, 9), range(10, 13), [13], range(21, 25))),
          np.concatenate((range(1, 9), range(10, 13), [17], [26], [29])),
          np.concatenate((range(1, 9), range(10, 13), [18], [27], [30])),
          np.concatenate((range(1, 9), range(10, 13), [19], [28], [31]))]

# the index of vars in vars_all
vars_id = vars_all.index(vars)

output_train = np.loadtxt(output_train_fn+vars+'_plumedata',
                          skiprows=1, usecols=range(2, 23))
output_val = np.loadtxt(output_val_fn+vars+'_plumedata',
                        skiprows=1, usecols=range(2, 23))
print(output_train.shape)
print(output_val.shape)
# output_val = output_train;
sys.path.append(rom_class_path)

nparam = len(ind_ip[vars_id])
noutput = 1
ind_op = 8  # considering the ind_op-th output
poly_deg = 5

for i in range(ind_op, ind_op+1):
    if only_nonallzero:
        # find the cases where outputs are not zeros
        tmp = np.nonzero(output_train)  # it is tuple of row indices and column indices
        nonallzero_ind_train = tmp[0][np.where(tmp[1] == ind_op)[0]]
        num_run_train = len(nonallzero_ind_train)  # number of successful runs
        print(nonallzero_ind_train)
        print(num_run_train)
    else:
        num_run_train = output_train.shape[0]
        nonallzero_ind_train = np.arange(num_run_train)

    if only_nonallzero:
        tmp = np.nonzero(output_val)  # it is tuple of row indices and column indices
        nonallzero_ind_val = tmp[0][np.where(tmp[1] == ind_op)[0]]
        num_run_val = len(nonallzero_ind_val)  # number of successful runs
    else:
        num_run_val = output_val.shape[0]
        nonallzero_ind_val = np.arange(num_run_val)

    ## numbers of the runs (second column in the plume data file)
    run_num_train = np.loadtxt(output_train_fn+vars+'_plumedata', skiprows=1, usecols=[1])
    run_num_train = run_num_train.astype(int)
    run_num_val = np.loadtxt(output_val_fn+vars+'_plumedata', skiprows=1, usecols=[1])
    run_num_val = run_num_val.astype(int)

    ### run_num_val = run_num_train;

    # load parameters
    param_train = np.loadtxt(param_train_fn)
    param_train = param_train.transpose()
    print(run_num_train[nonallzero_ind_train])
    print(ind_ip[vars_id])
    print(param_train.shape)
    param_train = param_train[np.ix_(run_num_train[nonallzero_ind_train], ind_ip[vars_id])]
    param_val = np.loadtxt(param_val_fn)
    param_val = param_val.transpose()
    param_val = param_val[np.ix_(run_num_val[nonallzero_ind_val],ind_ip[vars_id])]
    ### param_val = param_train

    sim_train = output_train;
    sim_train = sim_train[nonallzero_ind_train, ind_op]
    sim_val = output_val
    sim_val = sim_val[nonallzero_ind_val, ind_op]
    ### sim_val = sim_train;

    nsample_train = nonallzero_ind_train.shape[0];
    nsample_val = nonallzero_ind_val.shape[0];
    ### nsample_val = nsample_train;
    nsample_all = nsample_train+nsample_val;

# ## sklearn polynomial regression
# poly = PolynomialFeatures(degree=poly_deg)
# X_ = poly.fit_transform(param_train)
# predict_ = poly.fit_transform(param_val)
#
# clf = linear_model.LinearRegression()
# clf.fit(X_, sim_train)
# prediction = clf.predict(predict_)
# print(sim_val.shape)
# print(prediction.shape)
# results =  np.column_stack([sim_val, prediction])
# hd = 'true values             rom predictions'
# np.savetxt('sim_new_with_sklearn', results, header = hd)
# # np.savetxt('sim_val_sklearn', sim_val)
# # np.savetxt('sim_prediction_sklearn', prediction)
# # np.savetxt('sim_val_and_prediction_sklearn', np.concatenate(sim_val[]))

xgb_model = xgb.XGBRegressor(max_depth=4, n_estimators=100)
xgb_model.fit(param_train, sim_train)
sim_pred = xgb_model.predict(param_val)
print(sim_pred.shape)
print(sim_val.shape)
np.savetxt('sim_val_and_prediction_xgboost',
           np.column_stack((sim_val, sim_pred)))

os.chdir(wd_old)
print('Time elapsed:', time.time() - t0)
