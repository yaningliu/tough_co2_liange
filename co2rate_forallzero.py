## Does the scatter plot for co2ratem and pesandx for those cases where total volumes are all zeros and not all zeros

import string
import sys
import os
import numpy as np
from math import pow
import shutil
import scipy.io as sio
from mpi4py import MPI
import matplotlib.pyplot as plt

ntime = 21
cur_dir = os.getcwd()
foldern = '/clusterfs/lawrencium/yaningl/sim_new/sim_new_lowerco2rate_sz1200_013016'
os.chdir(foldern)

varn = ['ph', 'tds', 'ars', 'bar', 'cd', 'pb', 'ben', 'phe', 'ben']
data_filen = [x+'_plumedata' for x in varn]

tvol = []
for i in range(len(varn)):
    tvol.append(np.loadtxt(data_filen[i], skiprows=1, usecols=range(1,ntime+2)))

param_filen = 'parameters_mapped.txt'
param = np.loadtxt(param_filen)

# param = param[range(0,8)+range(9,31), :]
# param = param.transpose()

ind_zero = []
ind_nonzero = []
for i in range(len(varn)):
    ind_zero.append(np.where(~tvol[i][:,1:].any(axis=1))[0])
    ind_nonzero.append(np.where(tvol[i][:,1:].any(axis=1))[0])
print('ind_zero = ', ind_zero[0])
all_equal = True
for i in range(len(varn)):
    for j in range(len(varn)):
        if not np.array_equal(ind_zero[i], ind_zero[j]):
            all_equal = False

print('all_equal = ', all_equal)

ind_co2ratem = 0
ind_pesandx = 10

var_id = 0
x_zero = param[ind_co2ratem, tvol[var_id][ind_zero[var_id],0].astype(int)]
y_zero = param[ind_pesandx, tvol[var_id][ind_zero[var_id],0].astype(int)]
x_nonzero = param[ind_co2ratem, tvol[var_id][ind_nonzero[var_id],0].astype(int)]
y_nonzero = param[ind_pesandx, tvol[var_id][ind_nonzero[var_id],0].astype(int)]

colors = ['blue', 'red']
markers = ['o', 'x']

h1 = plt.scatter(x_zero, y_zero, color=colors[0], marker=markers[0])
h2 = plt.scatter(x_nonzero, y_nonzero, color=colors[1], marker=markers[1])
plt.legend((h1, h2), ('Zero', 'Nonzeros'))
plt.xlabel(r'$q_{co2}$', fontsize=20)
plt.ylabel('sand permeability', fontsize=20)
os.chdir(cur_dir)
plt.savefig('scatterplot_ph.png', format='png')

# tds
var_id = 1
x_zero = param[ind_co2ratem, tvol[var_id][ind_zero[var_id],0].astype(int)]
y_zero = param[ind_pesandx, tvol[var_id][ind_zero[var_id],0].astype(int)]
x_nonzero = param[ind_co2ratem, tvol[var_id][ind_nonzero[var_id],0].astype(int)]
y_nonzero = param[ind_pesandx, tvol[var_id][ind_nonzero[var_id],0].astype(int)]

colors = ['blue', 'red']
markers = ['o', 'x']

h1 = plt.scatter(x_zero, y_zero, color=colors[0], marker=markers[0])
h2 = plt.scatter(x_nonzero, y_nonzero, color=colors[1], marker=markers[1])
plt.legend((h1, h2), ('Zero', 'Nonzeros'))
plt.xlabel(r'$q_{co2}$', fontsize=20)
plt.ylabel('sand permeability', fontsize=20)
os.chdir(cur_dir)
plt.savefig('scatterplot_tds.png', format='png')
