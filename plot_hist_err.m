%% plot the convergence of eigenvalues

clear all;
var = {'ph', 'bar', 'pb', 'phe'};
data_path = '/global/cscratch1/sd/yaning/Research/Liange_sim_new/';
filen = {'PODfSBLgPCE_mo_ph_results_tra1val1po4tol4nb20.mat', ...
         'PODfSBLgPCE_mo_bar_results_tra1val1po4tol4nb20.mat', ...
         'PODfSBLgPCE_mo_pb_results_tra1val1po4tol4nb20.mat', ...
         'PODfSBLgPCE_mo_phe_results_tra1val1po4tol4nb20.mat'};
color = {'b', 'r'};
marker = {'o', 's'};
ls = {'-', ':'};
lw = 1.5;
markersize = 12;
fs = 20;
lgd = {'histogram'};

data = cell(length(var), 1);
for i = 1 : length(var)
    disp(['loading data of ' var{i}])
    data{i} = load([data_path filen{i}], 'err_pod');
end

for i = 1 : length(var)
    disp(['Dealing with ' var{i}])
    figure;

    yyaxis left;
    histogram(data{i}.err_pod, 50);
    set(gca, 'FontSize', fs);
    ylabel('Frequency')

    yyaxis right;
    boxplot(data{i}.err_pod, 'orientation', 'horizontal', 'Colors', 'k', ...
            'Symbol', 'm+')
    set(gca, 'YTick', [])
    l = legend(lgd);
    set(l, 'FontSize', fs, 'Location', 'northeast')
    % xlabel('Relative errors between TOUGH and SBLPCE')
    set(gca, 'FontSize', fs);
    l = legend(lgd);
    set(l, 'FontSize', fs, 'Location', 'northeast')
end