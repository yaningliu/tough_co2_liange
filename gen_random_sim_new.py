#!/usr/bin/python

import string
import sys
import os
from numpy import *
from math import pow
import shutil
import subprocess
import scipy.io as sio
import numpy as np

nparam = 31
noutput = 9
ntime = 20
nsample = int(sys.argv[1])
param_bound = np.zeros((nparam, 2))
param = np.zeros((nparam, nsample))

# q_co2
param_bound[0,0] = -5
param_bound[0,1] = -0.301
# q_BRN
param_bound[1,0] = -2.301
param_bound[1,1] = -1.125
# lambda
param_bound[2,0] = 0.2
param_bound[2,1] = 0.3
# T_1c
param_bound[3,0] = 5
param_bound[3,1] = 50
# dT_2c
param_bound[4,0] = 0
param_bound[4,1] = 100
# dT_3c
param_bound[5,0] = 5
param_bound[5,1] = 50
# T_1b
param_bound[6,0] = 1
param_bound[6,1] = 50
# dT_2b
param_bound[7,0] = 1
param_bound[7,1] = 10
# T_m, NOT USED HERE
param_bound[8,0] = 50
param_bound[8,1] = 200
# Porosity
param_bound[9,0] = 0.25
param_bound[9,1] = 0.5
# Sand Permeability
param_bound[10,0] = -14
param_bound[10,1] = -10
# Clay Permeability
param_bound[11,0] = -18
param_bound[11,1] = -15
# Pb concentration in the leaking brine
param_bound[12,0] = -8.12
param_bound[12,1] = -4.74
# Cd concentration in the leaking brine
param_bound[13,0] = -8.87
param_bound[13,1] = -6.43
# As concentration in the leaking brine
param_bound[14,0] = -9.0
param_bound[14,1] = -5.0
# Ba concentration in the leaking brine
param_bound[15,0] = -5.1
param_bound[15,1] = -2.3
# Benzene concentration in the leaking brine
param_bound[16,0] = -10.0
param_bound[16,1] = -3.2
# Phenol concentration in the leaking brine
param_bound[17,0] = -10.0
param_bound[17,1] = -3.7
# Naphtalene concentration in the leaking brine
param_bound[18,0] = -10.0
param_bound[18,1] = -4.1
# Cl concentration in the leaking brine
param_bound[19,0] = -2.0
param_bound[19,1] = -0.73
# Goethite volume fraction
param_bound[20,0] = 0.0
param_bound[20,1] = 0.1
# Illite volume fraction
param_bound[21,0] = 0.0
param_bound[21,1] = 0.15
# Kaolinite volume fraction
param_bound[22,0] = 0.0
param_bound[22,1] = 0.1
# Montmorillonite volume fraction
param_bound[23,0] = 0.0
param_bound[23,1] = 0.25
# Cation exchange capacity
param_bound[24,0] = 0.1
param_bound[24,1] = 40.0
# Benzene distribution coefficient
param_bound[25,0] = -4.5
param_bound[25,1] = 0.69
# Phenol distribution coefficient
param_bound[26,0] = -6.0
param_bound[26,1] = 0.15
# Naphthalene distribution coefficient
param_bound[27,0] = -3.1
param_bound[27,1] = 1.98
# Benzene degradation constant
param_bound[28,0] = -12
param_bound[28,1] = -6.1
# Phenol degradation constant
param_bound[29,0] = -12
param_bound[29,1] = -5.63
# Naphthalene degradation constant
param_bound[30,0] = -12
param_bound[30,1] = -6.45

## Generate random samples for input parameters
rand_num = np.random.random((nparam, nsample))
param = ( rand_num*(np.tile(param_bound[:,1:2], (1,nsample))-
                    np.tile(param_bound[:,0:1], (1,nsample))) +
          np.tile(param_bound[:,0:1], (1,nsample)) )

param_filen = 'parameters_mapped.txt'
param_normalized_filen = 'parameters_normalized.txt'

np.savetxt(param_filen, param, fmt='%.18e', delimiter=' ')
np.savetxt(param_normalized_filen, rand_num, fmt='%.18e', delimiter=' ')

#
# # cut hdmr parameters:
# cut_order = 2
# samp_order = 5
# nparam = 4
# noutput = 1
# iden = 'sim1d'
# pt_type = 'legl'
# ipl_type = 'lagr'
# param_filen = (iden + '_' + 'cut_hdmr_par_ind_pt_' + pt_type + '_ipl_' +
#                ipl_type + '_co' + str(cut_order) + '_so' + str(samp_order)
#                + '.mat')
#
# # reading the variables data from psuadeData
# param_data = sio.loadmat(param_filen)
# param_val = param_data['parameters']
# param_ind = param_data['par_ind']
# ni = param_val.shape[0]
# no = 1
# ns = param_val.shape[1]

for it in range(nsample):
  infile = open('flow-b.inp',"r")
  contentf = infile.read()
  infile.close()
  infile = open('chemical-b.inp',"r")
  contentc = infile.read()
  infile.close()

  var0 = pow(10, param[0,it])
  var0s = str("{0:.4e}".format(var0))  # string format with a postfix s
  var1 = pow(10, param[1,it])
  var1s = str("{0:.4e}".format(var1))
  var2 = var1 * param[2,it]
  var2s = str("{0:.4e}".format(var2))
  var3 = param[3,it]*365.24*86400
  var3s = str("{0:.5e}".format(var3))
  var4 = var3 + param[4,it]*365.24*86400
  var4s = str("{0:.5e}".format(var4))
  var5 = var4 + param[5,it]*365.24*86400
  var5s = str("{0:.5e}".format(var5))
  var6 = param[6,it]*365.24*86400
  var6s = str("{0:.5e}".format(var6))
  var7 = var6 + param[7,it]*365.24*86400
  var7s = str("{0:.5e}".format(var7))
  #var8 =
  #var8s = "{0:.5e}".format(var8)

  var9 = param[9,it]
  var9s = str("{0:7.3f}".format(var9))
  var10 = pow(10,param[10,it])
  var10_z = pow(10,param[10,it])/10
  var10s = str("{0:.3e}".format(var10))
  var10_zs = str("{0:.3e}".format(var10_z))
  var11 = pow(10,param[11,it])
  var11_z = pow(10,param[11,it])/10
  var11s = str("{0:.3e}".format(var11))
  var11_zs = str("{0:.3e}".format(var11_z))
  var12 = pow(10,param[12,it])
  var12g = pow(10,param[12,it])/10
  var12s = str("{0:.4e}".format(var12))
  var12gs = str("{0:.4e}".format(var12g))
  var13 = pow(10,param[13,it])
  var13g = pow(10,param[13,it])/10
  var13s = str("{0:.4e}".format(var13))
  var13gs = str("{0:.4e}".format(var13g))
  var14 = pow(10,param[14,it])
  var14g = pow(10,param[14,it])/10
  var14s = str("{0:.4e}".format(var14))
  var14gs = str("{0:.4e}".format(var14g))
  var15 = pow(10,param[15,it])
  var15g = pow(10,param[15,it])/10
  var15s = str("{0:.4e}".format(var15))
  var15gs = str("{0:.4e}".format(var15g))
  var16 = pow(10,param[16,it])
  var16s = str("{0:.4e}".format(var16))
  var17 = pow(10,param[17,it])
  var17s = str("{0:.4e}".format(var17))
  var18 = pow(10,param[18,it])
  var18s = str("{0:.4e}".format(var18))
  var19 = pow(10,param[19,it])
  var19s = str("{0:.4e}".format(var19))
  var20 = param[20,it]
  var20s = str("{0:.4f}".format(var20))
  var21 = param[21,it]
  var21s = str("{0:.4f}".format(var21))
  var22 = param[22,it]
  var22s = str("{0:.4f}".format(var22))
  var23 = param[23,it]
  var23s = str("{0:.4f}".format(var23))
  var24 = param[24,it]
  var24s = str("{0:.4f}".format(var24))
  var25 = pow(10,param[25,it])
  var25s = str("{0:.5e}".format(var25))
  var26 = pow(10,param[26,it])
  var26s = str("{0:.5e}".format(var26))
  var27 = pow(10,param[27,it])
  var27s = str("{0:.5e}".format(var27))
  var28 = pow(10,param[28,it])
  var28s = str("{0:.5e}".format(var28))
  var29 = pow(10,param[29,it])
  var29s = str("{0:.5e}".format(var29))
  var30 = pow(10,param[30,it])
  var30s = str("{0:.5e}".format(var30))

  contentf = contentf.replace('$co2ratem$',var0s)
  contentf = contentf.replace('$brnratem$',var1s)
  contentf = contentf.replace('$brnratet$',var2s)
  contentf = contentf.replace('$tico2rat1$',var3s)
  contentf = contentf.replace('$tico2rat2$',var4s)
  contentf = contentf.replace('$tico2rat3$',var5s)
  contentf = contentf.replace('$tibrnrat1$',var6s)
  contentf = contentf.replace('$tibrnrat2$',var7s)
  contentf = contentf.replace('$poros$',var9s)
  contentf = contentf.replace('$pesandx$',var10s)
  contentf = contentf.replace('$pesandy$',var10s)
  contentf = contentf.replace('$pesandz$',var10_zs)
  contentf = contentf.replace('$peclayx$',var11s)
  contentf = contentf.replace('$peclayy$',var11s)
  contentf = contentf.replace('$peclayz$',var11_zs)

  contentc = contentc.replace('$pbbrine$',var12s)
  contentc = contentc.replace('$pbbrinegue$',var12gs)
  contentc = contentc.replace('$cdbrine$',var13s)
  contentc = contentc.replace('$cdbrinegue$',var13gs)
  contentc = contentc.replace('$asbrine$',var14s)
  contentc = contentc.replace('$asbrinegue$',var14gs)
  contentc = contentc.replace('$babrine$',var15s)
  contentc = contentc.replace('$babrinegue$',var15gs)
  contentc = contentc.replace('$benbrine$',var16s)
  contentc = contentc.replace('$benbrinegue$',var16s)
  contentc = contentc.replace('$phebrine$',var17s)
  contentc = contentc.replace('$phebrinegue$',var17s)
  contentc = contentc.replace('$napbrine$',var18s)
  contentc = contentc.replace('$napbrinegue$',var18s)
  contentc = contentc.replace('$clbrine$',var19s)
  contentc = contentc.replace('$clbrinegue$',var19s)
  contentc = contentc.replace('$goethite$',var20s)
  contentc = contentc.replace('$illite$',var21s)
  contentc = contentc.replace('$kaolinite$',var22s)
  contentc = contentc.replace('$montmori$',var23s)
  contentc = contentc.replace('$cecsand$',var24s)
  contentc = contentc.replace('$benzenekd$',var25s)
  contentc = contentc.replace('$phenolkd$',var26s)
  contentc = contentc.replace('$naphthalkd$',var27s)
  contentc = contentc.replace('$bendecay$',var28s)
  contentc = contentc.replace('$phendecay$',var29s)
  contentc = contentc.replace('$naphdecay$',var30s)

  outfile = open('flow.inp',"w")       # generate the updated flow.inp file
  print >> outfile, '%s' %(contentf)
  outfile.close()
  outfile = open('chemical.inp',"w")   # generate the updated chemical.inp file
  print >> outfile, '%s' %(contentc)
  outfile.close()

  b = str(it)
  c = "run"+b
  # create a new directory
  os.mkdir(c)
  shutil.copy('chemical.inp',c)
  shutil.copy('flow.inp',c)
  shutil.copy('solute.inp',c)
  shutil.copy('tk-dem6.dat',c)
  shutil.copy('CO2TAB',c)
  shutil.copy('MESH',c)
  shutil.copy('INCON',c)
  shutil.copy('tr2.087_eco2n_lnx',c)

  os.remove('chemical.inp')
  os.remove('flow.inp')
